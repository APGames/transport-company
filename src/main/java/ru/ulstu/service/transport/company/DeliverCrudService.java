package ru.ulstu.service.transport.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.transport.company.Deliver;
import ru.ulstu.repository.transport.company.DeliverRepository;
import ru.ulstu.service.Crud;

import java.util.List;
import java.util.Optional;

@Service
public class DeliverCrudService implements Crud<Deliver> {
    private final DeliverRepository deliverRepository;

    public DeliverCrudService(DeliverRepository deliverRepository) {
        this.deliverRepository = deliverRepository;
    }

    @Override
    public Deliver create(Deliver deliver) {
        return deliverRepository.saveAndFlush(deliver);
    }

    @Override
    public List<Deliver> findAll() {
        return deliverRepository.findAll();
    }

    @Override
    public Deliver get(Integer id) {
        return deliverRepository.getOne(id);
    }

    @Override
    public Optional<Deliver> find(Integer id) {
        return deliverRepository.findById(id);
    }

    @Override
    public PageableItems<Deliver> findAll(int offset, int count) {
        final Page<Deliver> page = deliverRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public Deliver update(Deliver deliver) {
        return deliverRepository.saveAndFlush(deliver);
    }

    @Override
    public void delete(Deliver deliver) {
        deliverRepository.delete(deliver);
    }

    public List<Deliver> findAllByArrivedAfter(Long time) {
        return deliverRepository.findAllByArriveDateTimeAfter(time);
    }
}
