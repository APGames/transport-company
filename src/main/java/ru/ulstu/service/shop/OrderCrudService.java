package ru.ulstu.service.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.shop.Order;
import ru.ulstu.repository.shop.OrderRepository;
import ru.ulstu.service.Crud;

import java.util.List;
import java.util.Optional;

@Service
public class OrderCrudService implements Crud<Order> {
    private final OrderRepository orderRepository;

    public OrderCrudService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order create(Order order) {
        return orderRepository.saveAndFlush(order);
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order get(Integer id) {
        return orderRepository.getOne(id);
    }

    @Override
    public Optional<Order> find(Integer id) {
        return orderRepository.findById(id);
    }

    @Override
    public PageableItems<Order> findAll(int offset, int count) {
        final Page<Order> page = orderRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public Order update(Order order) {
        return orderRepository.saveAndFlush(order);
    }

    @Override
    public void delete(Order order) {
        orderRepository.delete(order);
    }
}
