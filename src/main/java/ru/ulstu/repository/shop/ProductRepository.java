package ru.ulstu.repository.shop;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.shop.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}
