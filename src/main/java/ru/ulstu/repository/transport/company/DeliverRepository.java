package ru.ulstu.repository.transport.company;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.transport.company.Deliver;
import ru.ulstu.model.transport.company.Transport;

import java.util.List;

public interface DeliverRepository extends JpaRepository<Deliver, Integer> {
    List<Deliver> findAllByArriveDateTimeAfter(Long time);
}
