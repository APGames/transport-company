package ru.ulstu.repository.transport.company;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.transport.company.Driver;

public interface DriverRepository extends JpaRepository<Driver, Integer> {
}
