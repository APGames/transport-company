package ru.ulstu.model.transport.company;

import org.springframework.lang.NonNull;
import ru.ulstu.model.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "driver", indexes = {
        @Index(name = "driver_index", columnList = "id")
})
public class Driver extends BaseEntity {
    @OneToMany(mappedBy = "driver")
    private Set<Transport> transports;

    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    @NonNull
    private String passport;

    public Set<Transport> getTransports() {
        return transports;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }
}
