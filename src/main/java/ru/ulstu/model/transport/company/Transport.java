package ru.ulstu.model.transport.company;

import org.springframework.lang.NonNull;
import ru.ulstu.model.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "transport", indexes = {
        @Index(name = "driver_id_index", columnList = "driver_id"),
        @Index(name = "transport_index", columnList = "id")
})
public class Transport extends BaseEntity {
    @OneToMany(mappedBy = "transport")
    private Set<Deliver> delivers;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "driver_id", nullable = false)
    private Driver driver;

    @NonNull
    private String carNumber;
    @NonNull
    private String carModelName;

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarModelName() {
        return carModelName;
    }

    public void setCarModelName(String carModelName) {
        this.carModelName = carModelName;
    }

    public Set<Deliver> getDelivers() {
        return delivers;
    }
}
