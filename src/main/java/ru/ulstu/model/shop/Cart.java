package ru.ulstu.model.shop;

import org.springframework.lang.NonNull;
import ru.ulstu.model.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "cart")
public class Cart extends BaseEntity {
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @NonNull
    private Integer amount;

    private String promoCode;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "order_id")
    private Order order;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promocode) {
        this.promoCode = promocode;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
