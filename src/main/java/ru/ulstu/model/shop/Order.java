package ru.ulstu.model.shop;

import org.springframework.lang.NonNull;
import ru.ulstu.model.BaseEntity;
import ru.ulstu.model.transport.company.Deliver;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "shop_order")
public class Order extends BaseEntity {
    @OneToMany(mappedBy = "order")
    private Set<Cart> carts;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "deliver_id")
    private Deliver deliver;

    @NonNull
    private Long orderDateTime;
    @NonNull
    private Long arrivedDateTime;
    @NonNull
    private String toAddress;
    @NonNull
    private Boolean isHomeDelivery;

    public Set<Cart> getCarts() {
        return carts;
    }

    public Deliver getDeliver() {
        return deliver;
    }

    public void setDeliver(Deliver deliver) {
        this.deliver = deliver;
    }

    public Long getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(Long orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public Long getArrivedDateTime() {
        return arrivedDateTime;
    }

    public void setArrivedDateTime(Long arrivedDateTime) {
        this.arrivedDateTime = arrivedDateTime;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public Boolean getHomeDelivery() {
        return isHomeDelivery;
    }

    public void setHomeDelivery(Boolean homeDelivery) {
        isHomeDelivery = homeDelivery;
    }
}
